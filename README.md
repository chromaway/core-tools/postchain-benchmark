# Postchain benchmark

This microperformance benchmark suite is built using JMH https://github.com/openjdk/jmh

The test suite comes with some defaults but everything can be overridden according to JMH API.

## Prerequisites

### Docker
Running the `postchain-benchmark` docker image requires no prior installations.

### Native
If you run natively using the tar dist you need to install Java and PostgreSQL. 
Set up PostgreSQL according to instructions in docs:
https://docs.chromia.com/getting-started/dev-setup/database-setup

## What do we benchmark?

The benchmarking test is based on `GTXPerformanceSlowIntegrationTest` in postchain repository.
We benchmark how long it takes to build and commit a block.

## Parameters

You can configure the following parameters for the block building test:

| Name        | Default | Valid values | Description                  |
|-------------|---------|--------------|------------------------------|
| nodeCount   | 1,4     | 1, 3, 4, 10  | Number of signer nodes       |
| txPerBlock  | 1000    | 1 .. 1000    | Transactions to put in block |
| opsPerBlock | 1000    | 1 ..         | Operations per transaction   |

## Benchmark JMH settings

- Warmup: 1 iterations, 15 s each
- Measurement: 1 iterations, 30 s each
- Benchmark mode: Average time, time/op
- Forks: 3

## How to interpret results

```
2023-12-08 15:14:12 Benchmark                                  (nodeCount)  (opPerTx)  (txPerBlock)  Mode  Cnt    Score     Error  Units
2023-12-08 15:14:12 PostchainBenchmark.benchmarkBlockBuilding            1          1          1000  avgt    3  488.119 ± 135.155  ms/op
2023-12-08 15:14:12 PostchainBenchmark.benchmarkBlockBuilding            4          1          1000  avgt    3  958.495 ± 162.621  ms/op
```

Standard test setup runs one benchmark with single node and one with 4 nodes.

A performant node should be able to run **single node** around ~1000 ms/op.\
For **4 nodes** it should manage with not too much more than double the time for single node, so somewhere around ~2000 - 2500 ms/op.

If error margin is high you may want to tweak execution (longer warmup, longer tests etc.).
Please refer to JMH documentation and examples.
