package net.postchain.benchmark

import net.postchain.common.BlockchainRid
import net.postchain.concurrent.util.get
import net.postchain.crypto.KeyPair
import net.postchain.crypto.devtools.KeyPairHelper
import net.postchain.devtools.IntegrationTestSetup
import net.postchain.devtools.PostchainTestNode
import net.postchain.ebft.worker.ValidatorBlockchainProcess
import net.postchain.gtv.GtvFactory
import net.postchain.gtx.GtxBuilder
import org.openjdk.jmh.annotations.Benchmark
import org.openjdk.jmh.annotations.BenchmarkMode
import org.openjdk.jmh.annotations.Fork
import org.openjdk.jmh.annotations.Level
import org.openjdk.jmh.annotations.Measurement
import org.openjdk.jmh.annotations.Mode
import org.openjdk.jmh.annotations.OutputTimeUnit
import org.openjdk.jmh.annotations.Param
import org.openjdk.jmh.annotations.Scope
import org.openjdk.jmh.annotations.Setup
import org.openjdk.jmh.annotations.State
import org.openjdk.jmh.annotations.TearDown
import org.openjdk.jmh.annotations.Warmup
import java.lang.Exception
import java.util.concurrent.TimeUnit

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 1, time = 15, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 1, time = 30, timeUnit = TimeUnit.SECONDS)
@Fork(3)
@State(Scope.Benchmark)
open class PostchainBenchmark : IntegrationTestSetup() {

    private val supportedNodeCounts = setOf(1, 3, 4, 10)
    private val sigMaker = cryptoSystem.buildSigMaker(KeyPair(KeyPairHelper.pubKey(0), KeyPairHelper.privKey(0)))

    @Param("1", "4")
    var nodeCount = 0

    // Why make these params if we only have one value?
    // Well, you can override when running, this is just defaults
    @Param("1000")
    var txPerBlock = 0

    @Param("1")
    var opPerTx = 0

    @Setup
    fun startNodes() {
        if (!supportedNodeCounts.contains(nodeCount)) {
            throw Exception("$nodeCount is not supported for 'nodeCount' parameter. Supported values are: $supportedNodeCounts")
        }

        configOverrides.setProperty("testpeerinfos", createPeerInfos(nodeCount))
        createNodes(nodeCount, "/net/postchain/devtools/performance/blockchain_config_$nodeCount.xml")

        // Build genesis block first
        buildBlock(PostchainTestNode.DEFAULT_CHAIN_IID)
    }

    @Setup(Level.Invocation)
    fun injectTxs() {
        val blockchainRid = systemSetup.blockchainMap[1]!!.rid

        val statusManager = (nodes[0].getBlockchainInstance() as ValidatorBlockchainProcess).statusManager
        val primaryNode = nodes[statusManager.primaryIndex()]
        val engine = primaryNode
                .getBlockchainInstance()
                .blockchainEngine
        val txFactory = engine
                .getConfiguration()
                .getTransactionFactory()

        val queue = (primaryNode.getBlockchainInstance() as ValidatorBlockchainProcess).networkAwareTxQueue

        var txId = engine.getBlockQueries().getLastBlockHeight().get() * txPerBlock + 1
        val txs = (1..txPerBlock).map { makeTestBatchTx(txId++, opPerTx, blockchainRid) }
        txs.forEach { queue.enqueue(txFactory.decodeTransaction(it)) }
    }

    @Benchmark
    fun benchmarkBlockBuilding() {
        buildBlock(PostchainTestNode.DEFAULT_CHAIN_IID)
    }

    @TearDown
    fun stopNodes() {
        super.tearDown()
    }

    private fun makeTestBatchTx(txCounter: Long, opCount: Int, blockchainRid: BlockchainRid): ByteArray {
        val b = GtxBuilder(blockchainRid, listOf(KeyPairHelper.pubKey(0)), cryptoSystem)

        for (i in 0 until opCount) {
            b.addOperation("gtx_test", GtvFactory.gtv(1L), GtvFactory.gtv((txCounter * opCount + i).toString()))
        }

        return b.finish()
                .sign(sigMaker)
                .buildGtx()
                .encode()
    }

}
