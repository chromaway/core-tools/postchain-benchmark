#!/bin/sh
# Copyright (c) 2022 ChromaWay Inc. See README for license information.

set -eu

echo "Starting postgres..."
mkdir -p "$PGDATA"
chown -R postgres "$PGDATA"
chmod 700 "$PGDATA"

mkdir -p /var/run/postgresql
chown -R postgres /var/run/postgresql
chmod 775 /var/run/postgresql

su-exec postgres initdb "$POSTGRES_INITDB_ARGS"
su-exec postgres pg_ctl start
su-exec postgres psql -c "CREATE USER $POSTGRES_USER;"
su-exec postgres psql -c "ALTER USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD';"
su-exec postgres psql -c "CREATE DATABASE $POSTGRES_DB"
su-exec postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;"

echo "Starting benchmark..."
exec java -cp @/app/jib-classpath-file org.openjdk.jmh.Main "$@"
